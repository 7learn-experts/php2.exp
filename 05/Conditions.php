<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

// include "../common/common.php";
// include_once "../common/common.php";
// require "../common/common.php";
require_once "../common/common.php";

myPrint(include "config.php");

$age = rand(0,120);
$contentUnder18 = "The -18 Content";
$contentUnder50 = "The 18-50 Content";
$contentAbove50 = "The +50 Content";


echo "Generated Age : {$age}<br>";
if($age < 18){
    myPrint($contentUnder18) ;
}else if($age < 50){
    myPrint($contentUnder50);
}else{
    myPrint($contentAbove50);
}

$result = ($age < 18) ? $contentUnder18 : ($age < 50) ? $contentUnder50 : $contentAbove50 ;
myPrint($result);

/* Your Work : Convert above if to switch :
switch ($age){
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
        echo $contentUnder18;
        break;
    case 18:
    case 19:
    case 20:
        break;
    default:
        echo "Invalid Age";
}
*/

// continue ..
myPrint("<hr>");
//var_dump($_REQUEST);

$num1 = rand(0,99);
$num2 = rand(0,99);
$op = "-";
myPrint("$num1 {$_REQUEST['op']} $num2 = ");

switch ($_REQUEST['op']){
    case "+" :
        echo $num1 + $num2 ;
        break;
    case "-" :
        echo $num1 - $num2 ;
        break;
    case "*" :
        echo $num1 * $num2 ;
        break;
    case "/" :
        echo $num1 / $num2 ;
    break;
    default:
        echo "InvalidOperation" ;
}

