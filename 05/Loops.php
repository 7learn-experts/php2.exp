<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once "../common/common.php";

$arr = [3,4,5,7,"dododo"=>222,45];

myPrint("Array Values : ");
myPrint($arr);

myPrint("Array Print with While : ");

$i = 0;
while ($i < sizeof($arr)){
    myPrint($arr[$i],"inline");
    $i++;
}

myPrint("Array Print with Do..While : ");
$i = 0;
do{
    myPrint($arr[$i],"inline");
    $i++;
}while($i < sizeof($arr));

myPrint("Array Print with For : ");
for($i = 0;$i < sizeof($arr);$i++){
    myPrint($arr[$i],"inline");
}

myPrint("Array Print with Foreach : ");
foreach ($arr as $key => $value){
    myPrint("$key:$value","inline");
}

myPrint("------------------------------");
// mazrab 3
for ($n = 0;$n < 100 ; $n += 3){
    myPrint($n,"inline");
}
myPrint("----------Multi Dimensional Array---------");
$arr2D0 = [
    [1,2,3,4],
    [12,22]
];

$arr3D = [
    [[1.1,1.2,1.3],[2.1,2.2,2.3]],  // 0
    [[1.1,1.2,1.3],[2.1,2.2,2.3]],  // 1
];
foreach ($arr3D as $arr2D){
    foreach ($arr2D as $arr1D){
       foreach ($arr1D as $member){
            echo "$member,";
       }
        echo "<br>";
    }
}
