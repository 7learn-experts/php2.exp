<?php
include '../common/common.php';
$dbInfo = new stdClass;
$dbInfo->host = 'localhost';
$dbInfo->name = 'php2_simple_blog';
$dbInfo->user = 'root';
$dbInfo->password = '';

$mysqli = new mysqli($dbInfo->host, $dbInfo->user, $dbInfo->password, $dbInfo->name);
//var_dump($mysqli);
if($mysqli->connect_errno){
    myPrint($mysqli->connect_error,null,'error');
}
$mysqli->set_charset('utf8');
//$mysqli->query("set names utf8;");