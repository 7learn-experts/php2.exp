<?php
// Options Table
// Columns : id, option_name, option_value


$footer_text = "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد. 
لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.";

//$bgColor = 'rgb('.rand(200,255).','.rand(200,255).','.rand(200,255).')';
$bgColor = '#ffffff';
$cssFiles = '
    <link rel="stylesheet" href="'.ROOT_URL.'milligram/css/normalize.css">
    <link rel="stylesheet" href="'.ROOT_URL.'milligram/css/milligram-rtl.css">
    <link rel="stylesheet" href="'.ROOT_URL.'milligram/css/custom.css">
';

$jsFiles = '
    <script src="'.ROOT_URL.'milligram/js/jquery.min.js"></script>
    <script src="'.ROOT_URL.'milligram/js/common.js"></script>
';

$widgets = [
    ["id"=>"auth","title" => "ورود و عضویت", "body" => '
    <form action="authProcess.php?action=login" method="post" class="ajaxSubmit">
    <div>
        <input type="text" name="username" >
        <input type="password" name="password" >
        <input type="checkbox" value="1" name="remember"> مرا به خاطر بسپار.
    </div>
    <input type="submit" value="ورود">
    <div class="result" style="display: none">
        <img src="../milligram/img/ld1.gif" alt="Loading" style="width: 128px">
    </div>
</form>
    '],
    ["id"=>"search","title" => "جستجو", "body" => "Html search Form"],
    ["id"=>"archive","title" => "آرشیو", "body" => "آرشیو وبسایت من"],
    ["id"=>"categories","title" => "دسته بندی", "body" => "دسته بندی ها"],
];


$selectQuery = "SELECT * from posts";
if($result = $mysqli->query($selectQuery)){
    $allPosts = $result->fetch_all(MYSQLI_ASSOC);
}else{
    printArr($mysqli->error_list[0]);
}

/*foreach ($allPosts as $p){
    $sql = "INSERT INTO `posts` (`author`, `access_level`, `title`, `body`) VALUES ('1', '{$p['access_level']}', '{$p['title']}', '{$p['body']}');";
    $mysqli->query($sql);
}*/


$allComments = [
    ["id"=>1,"post_id"=>11,"author"=>"لقمان آوند","body"=>"مرسی مطلبتون خیلی خوب بود"],
    ["id"=>2,"post_id"=>11,"author"=>"علی علوی","body"=>"مرسی مطلبتون خیلی خوب بود"],
    ["id"=>3,"post_id"=>11,"author"=>"لقمان آوند","body"=>"مرسی مطلبتون خیلی خوب بود"],
    ["id"=>4,"post_id"=>22,"author"=>" آوند","body"=>"مرسی مطلبتون خیلی خوب بود"],
    ["id"=>5,"post_id"=>22,"author"=>"لقمان ","body"=>"مرسی مطلبتون خیلی خوب بود"],
];