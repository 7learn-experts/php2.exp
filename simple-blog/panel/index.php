<?php include_once "../config.php";  ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?php echo SITE_TITLE ;?>
    </title>

    <?php   // fix this
        echo $cssFiles;
        echo $jsFiles;
    ?>

</head>
<body style="background: <?php echo $bgColor; ?>">
<div class="container">

    <div class="row">
        <div class="column header"><?php echo SITE_TITLE ;?></div>
    </div>

    <div class="row">
        <div class="column column-25">
            <a class="button button-fw" href="posts.php">مقالات</a>
            <a class="button button-fw" href="comments.php">کامنت ها</a>
            <a class="button button-fw" href="users.php">کاربرها</a>
            <a class="button button-fw" href="widgets.php">ویجت ها</a>
            <a class="button button-fw" href="options.php">تنظیمات</a>
        </div>

        <div class="column ">
            Body
        </div>

    </div>
</div>

</body>
</html>