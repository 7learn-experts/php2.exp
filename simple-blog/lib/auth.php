<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);

$users = [
    'admin' => ['fullname' => 'Loghman Avand', 'age' => 29, 'password' => 'e10adc3949ba59abbe56e057f20f883e'],
    'ali8'  => ['fullname' => 'Ali Hasht', 'age' => 35, 'password' => 'b1c783ddbf6da5dbd7d1c4233e3299dc']
];
if(!function_exists('userExists'))
{
    function userExists($username){
        global $users;
        return array_key_exists($username,$users);
    }
}

function currentUsername(){
    return $_SESSION['login'] ?? null;
}

function isLogin()
{
    if(!empty($_COOKIE['remember'])){
        if(userExists($_COOKIE['remember'])){
            $_SESSION['login'] = $_COOKIE['remember'];
        }
    }
    return $_SESSION['login'] ?? false;
}

function login($username, $password,$remember = 0)
{
    global $users;
    if (userExists($username)) {
        $user = $users[$username];
        if ($user['password'] == strtolower(md5($password))){
            $_SESSION['login'] = $username;
            if(intval($remember) == 1){
                setcookie('remember', $username , time() + 3600 * 24 * 14 );
            }
            return true;
        }
    }
    return false;
}

function logout(){
    unset($_SESSION['login']);
    setcookie('remember', "" , time() - 9000 );
    session_destroy();
}