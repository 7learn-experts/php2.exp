<?php include_once "config.php";  ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?php echo SITE_TITLE ;?>
    </title>

    <?php
        echo $cssFiles;
        echo $jsFiles;
    ?>


</head>
<body style="background: <?php echo $bgColor; ?>">
<div class="container">

    <div class="row">
        <div class="column header"><?php echo SITE_TITLE ;?></div>
    </div>

    <div class="row">
        <div class="column column-25">
            <?php foreach ($widgets as $wg): ?>

            <?php if ( ($wg['id'] != "auth") or ($wg['id'] == "auth" and !$isLogin) ): ?>
            <div class="widget widget-<?php echo $wg['id'] ?>">
                <div class="widget-title"><?php echo $wg['title'] ?></div>
                <div class="widget-body"><?php echo $wg['body'] ?></div>
            </div>
            <?php else: ?>
            <div class="widget widget-login">
                <div class="widget-title">لاگین شدید</div>
                <div class="widget-body">
                    <a class="button" href="http://php2.exp/simple-blog/?filter=author&author=<?php echo currentUsername(); ?>">پست های من</a><br>
                    <a class="button button-red" href="http://php2.exp/simple-blog/authProcess.php?action=logout">خروج</a>
                </div>
            </div>

                <?php endif; ?>

            <?php endforeach; ?>
        </div>

        <div class="column ">
            <?php foreach ($posts as $p): ?>
            <div class="postBox"">
                <div class="postBox-title">
                    <a href="single.php?post=<?php echo $p['id']; ?>"><?php echo $p['title']; ?></a><br>
                    <a href="http://php2.exp/simple-blog/?filter=author&author=<?php echo $p['author']; ?>"><?php echo $p['author']; ?></a><br>
                </div>
                <?php if(($p['access_level'] == 'public') or ($p['access_level'] == 'member' and $isLogin) ): ?>
                <div class="postBox-body"><?php echo get_excerpt($p); ?></div>
                <button class="button like-btn" data-pid="<?php echo $p['id']; ?>">Like</button>
                <button class="button dislike-btn" data-pid="<?php echo $p['id']; ?>">Dislike</button>
                <?php else: ?>
                    <div class="postBox-body access_locked">
                        <img src="assets/img/locked.png" alt="access locked"><br>
                        محتوا فقط برای اعضای سایت قابل نمایش است.
                    </div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
            <?php if($currentPage <= $pageCount): ?>
            <div class="pagination">
                <a class="button" href="?page=1">اول</a>
                <?php for ($i=1; $i<=$pageCount;$i++): ?>
                <?php
                // bug fix needed !
                    if(strpos($_SERVER['QUERY_STRING'],"?page=") !== false){
                        $qs = str_replace("?page=?","?",$qs);
                    }else{
                        $qs = "?page=$i&{$_SERVER['QUERY_STRING']}";
                    }

                    ?>
                    <a class="button" href="<?php echo $qs; ?>"><?php echo $i; ?></a>
                <?php endfor;?>
                <a class="button" href="?page=<?php echo $pageCount; ?>">آخر</a>
            </div>
            <?php else: ?>
            <div class="error">چنین صفحه ای در سایت موجود نیست.</div>
            <?php endif; ?>

        </div>

    </div>

    <div class="row">
        <div class="column footer"><?php echo $footer_text ;?></div>
    </div>
</div>

</body>
</html>