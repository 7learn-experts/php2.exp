<?php
include_once "../common/common.php";

$globalVar = "Global Variable";


function counter(){
    static $i = 0;
    $i++;
    myPrint($i);
}
counter();
counter();
counter();
counter();
counter();

function f(){
    global $globalVar;
//    global $wpdb;
    $localVar = "Local Variable";
    $age = 15;
    g();
    $name = "Ali";
    echo "f()";
    var_dump($globalVar,$localVar);
}

function g(){
    $a = 1;
    $b = 1;
}

var_dump($globalVar,$localVar);
f();

/* Stack
------------ top

------------
*/

$queue = [3,4,5,6]; // top
