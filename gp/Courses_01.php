
<?php
include_once "My-Functions.php";
 
    $math = post_value('math',0);
    $prog = post_value('prog',0);

  if ($math) {
        if ($prog) {
            $title = 'programming and mathematics courses';
        } else {
            $title = 'mathematics courses';
        }
        
  } else {
      if ($prog) {
            $title = 'programming courses';
      } else {
            $title = 'courses';
      }
      
  }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>
</head>
<body>
    <h1 style="color: #e15342;">
        <p> <?php myPrint('Select category courses :'); ?></p>
    </h1>
    <?php if($math):?>
        <div class="mathematics" >
            <h2><?php myPrint('Mathematics courses :'); ?></h2>
            <ol style="font-size:20px">
                <li>General Mathematics</li>
                <li>Linear Algebra</li>
                <li>Numerical Methods</li>
                <li>Discrete Mathematics</li>
                <li>Engineering Mathematics</li>
            </ol>
        </div>
    <?php endif; ?>
    
    <?php if($prog):?>
        <div class="program">
            <h2><?php myPrint('Programming courses :'); ?></h2>
            <ol style="font-size:20px">
                <li> C/C++ Programming </li>
                <li> C# Programming </li>
                <li> Java Programming </li>
                <li> PHP programming </li>
                <li> MATLAB programming </li>
                <li> PHP programming </li>
            </ol>
        </div>
    <?php endif; ?>

    <?php if(!$prog && !$math):?>
        <h2 style="color: #e15342;">
            <?php myPrint('Sorry, There is not any courses for you, right now.!');?> 
        </h2>    
    <?php endif; ?>
    
    <div>
        <button style="background-color: #4CAF50;  border: none; padding: 15px 32px; text-align: center;">
        <a style="text-decoration:none; color:white; font-size: 15px;" href="Request_courses_01.php">Return to Request Form</a>
        </button>
    </div>
 
</body>
</html> 