<?php

// $text = 'Welcome to PHP! Do you like PHP?';
$text = '<a href="#">Link</a>';

$a = urlencode($text);

$b = urldecode($a);

echo '<p>Original text: '.$text.'</p>';
echo '<p>After urlencode: '.$a.'</p>';
echo '<p>After urldecode: '.$b.'</p>';
