<?php
function myPrint($data,$printType = null){
    if(is_array($data)){
        printArr($data);
    }else{
        if($printType == 'inline'){
            echo "<div style='background-color: #eee;padding: 5px;border-radius: 5px;margin: 5px ;display: inline-block;'>$data</div>";
        }else{
            echo "<div style='background-color: #eee; padding: 5px ; border-radius: 5px; margin: 5px;'>$data</div>";
        }
    }
}

function printArr($arr)
{
    foreach ($arr as $k => $v) {
        echo "<div style='background-color: #eee;padding: 5px;border-radius: 5px;margin: 5px ;display: inline-block;'>$k:$v</div>";
    }
}

function islogin(){
    return rand(0,1);
}


function get_value($param, $default = null)
{
    if (isset($_GET[$param])) {
        $value = $_GET[$param];
    } else {
        $value = $default;
    }
    return $value;
}

function post_value($param, $default = null)
{
    if (isset($_POST[$param])) {
        $value = $_POST[$param];
    } else {
        $value = $default;
    }
    return $value;
}


function convert($num,$coef_1,$coef_2)
{
    return $num*$coef_1/$coef_2;
}