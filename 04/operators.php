<?php
$age = 29;
$Age = 29;
$name = "Loghman";
$url = "www.7Learn.com";
$pi = 3.14;
$single = false;
$numbers1 = [11,3,5,67,7,5];
$numbers2 = array(11,3,5,67,7,5);
$arr = [
    "yek"=>[1.12,2,"ali"],         // 0
    [1,array(3,7,4,5,"shish"=>6,7),3],     // 1
    false,                  // 2
    "ali" => 77                      // 3
];
//$arr[1][1][4] = 6666;
//echo $arr[1][1]["shish"];
//echo $arr["yek"][2];

// single-line Comment
# single-line Comment
/*
 Multi-line Comment
*/

$money = NULL;
$for = 1;

$tmp = $arr2D[1];


$var1 = "5";
$var2 = "5";
// echo gettype($var2);
var_dump($var1 == $var2);
var_dump($var1 === $var2);

$find = stripos("7Learn Student are awesome","7Learn");
if($find === false){
    echo "Not found";
}else{
    echo "Found at index $find";
}

$age = 16;
//var_dump(--$age);
var_dump(array(1,2) XOR true);


/*
OR , || :
-----------------
1 or 1 = 1
1 or 0 = 0 or 1 = 1
0 or 0 = 0

AND , && :
-----------------
1 and 1 = 1
1 and 0 = 0 or 1 = 0
0 and 0 = 0

XOR :
-----------------
1 xor 1 = 0
1 xor 0 = 0 or 1 = 1
0 xor 0 = 0

*/