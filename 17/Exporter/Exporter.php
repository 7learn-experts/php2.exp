<?php
abstract class Exporter{
    protected $data = null;
    protected $table = null;
    protected static $extension = null;

    public function __construct($tableName)
    {
        $this->import($tableName);
    }

    public function import($tableName){
        $className = ucfirst(substr($tableName,0,strlen($tableName) - 1)) ;
        $classPath = "../../16/ORM/$className.php";
        if(file_exists($classPath) and is_readable($classPath)){
            include $classPath;
            $model = new $className();
            $this->data = (array)$model->all();
            $this->table = $tableName;
        }else{
            echo "Invalid Request !";
        }

    }
    public abstract function export($filePath = null);
}

class TEXT_Exporter extends Exporter {
    protected static $extension = 'txt';
    public function export($filePath = null)
    {
        $str = "";
        foreach ($this->data as $record){
                $str .= implode(",",(array)$record) . PHP_EOL;
        }
        if(is_null($filePath)){
            $filePath = "$this->table-".rand(999,9999).".".static::$extension;
        }
        file_put_contents("files/$filePath",$str);
        echo "Successful Export as " . static::$extension;
    }
}
class CSV_Exporter extends TEXT_Exporter
{
    protected static $extension = 'csv';
}

class JSON_Exporter extends Exporter {
    protected static $extension = 'json';
    public function export($filePath = null)
    {
        $str = json_encode($this->data);
        if(is_null($filePath)){
            $filePath = "$this->table-".rand(999,9999).".".static::$extension;
        }
        file_put_contents("files/$filePath",$str);
        echo "Successful Export as " . static::$extension;
    }
}
/*
$txtExporter = new TEXT_Exporter();
$txtExporter->import('users');
$txtExporter->export();


$csvExporter = new CSV_Exporter();
$csvExporter->import('users');
$csvExporter->export();


$jsonExporter = new JSON_Exporter();
$jsonExporter->import('users');
$jsonExporter->export();

$jsonExporter = new JSON_Exporter('users');
$jsonExporter->export();

*/
