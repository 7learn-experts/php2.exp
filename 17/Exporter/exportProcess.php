<?php
require "Exporter.php";
//var_dump($_SERVER);
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "form not submitted !";
    die();
}

$whilelist = ['users', 'posts'];
if (in_array($_POST['export_table'], $whilelist)) {
    $className = "{$_POST['export_type']}_Exporter";
    $exporter  = new $className($_POST['export_table']);
    $exporter->export();
} else {
    echo "Cannot Export from '{$_POST['export_table']}' table!";
}
/*
$jsonExporter = new JSON_Exporter('users');
$jsonExporter->export();
*/